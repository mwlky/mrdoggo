using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Code.UI
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField] private Image fillImage;
        [SerializeField] private Image backgroundImage;

        [Inject] private Camera _camera;
        
        public void UpdateHealthBar(float currentHp, float maxHp)
        {
            float value = currentHp / maxHp;
            fillImage.fillAmount = value;
        }

        private void Update()
        {
            fillImage.gameObject.transform.LookAt(_camera.transform);
            backgroundImage.gameObject.transform.LookAt(_camera.transform);
        }
    }
}