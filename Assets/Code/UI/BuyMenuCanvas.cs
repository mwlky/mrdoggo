using System;
using System.Collections;
using System.Globalization;
using Code.Classes;
using Code.Input;
using Code.Managers;
using Code.Player;
using TMPro;
using UnityEngine;
using Zenject;

namespace Code.UI
{
    public class BuyMenuCanvas : MonoBehaviour
    {
        public static event Action OnBuyMenuHidden;
        
        [SerializeField] private GameObject[] buyMenuObjects;
        [Space] 
        
        [Header("Texts")]
        [SerializeField] private TextMeshProUGUI healthText;
        [SerializeField] private TextMeshProUGUI attackText;
        [SerializeField] private TextMeshProUGUI goldText;
        [SerializeField] private TextMeshProUGUI hpUpgradeCostText;
        [SerializeField] private TextMeshProUGUI attackUpgradeCostText;

        [Header("Warning Canvas")]
        [SerializeField] private GameObject warningCanvas;
        [Space]
        
        [Header("Animator")] 
        [SerializeField] private Animator buyMenuAnimator;
        [Space] 
        
        [SerializeField] private float canvasDisableDelay = 1.5f;

        [Inject] private UpgradeManager _upgradeManager;
        [Inject] private CurrencyManager _currencyManager;
        [Inject] private PlayerStatsUpgrader _playerStats;

        private void OnEnable()=>
            ShowBuyMenu.OnOpenBuyMenu += ShowMenu;

        private void OnDisable() =>
            ShowBuyMenu.OnOpenBuyMenu -= ShowMenu;
        
        private void ShowMenu()
        {
            foreach (GameObject buyMenuObject in buyMenuObjects)
                buyMenuObject.SetActive(true);
            
            UpdateTexts();
            buyMenuAnimator.SetBool(Constructors.BUY_MENU_OPEN, true);
        }

        private void UpdateTexts()
        {
            healthText.text = _playerStats.CurrentPlayerHealth.ToString(CultureInfo.CurrentCulture);
            attackText.text = _playerStats.CurrentPlayerAttack.ToString(CultureInfo.CurrentCulture);
            goldText.text = _currencyManager.CurrentGold.ToString(CultureInfo.CurrentCulture);

            hpUpgradeCostText.text = _upgradeManager.GetHealthUpgradeCost().ToString(CultureInfo.CurrentCulture);
            attackUpgradeCostText.text = _upgradeManager.GetAttackUpgradeCost().ToString(CultureInfo.CurrentCulture);
        }

        private void HideMenu()
        {
            OnBuyMenuHidden?.Invoke();
            buyMenuAnimator.SetBool(Constructors.BUY_MENU_OPEN, false);
            warningCanvas.SetActive(false);

            StartCoroutine(HideMenuCoroutine());
        }

        private IEnumerator HideMenuCoroutine()
        {
            yield return new WaitForSeconds(canvasDisableDelay);
            
            foreach (GameObject buyMenuObject in buyMenuObjects)
                buyMenuObject.SetActive(false);
        }

        public void HealthUpgrade()
        {
            if (_upgradeManager.TryUpgradePlayerHealth())
            {
                UpdateTexts();
                return;
            }

            ShowNotEnoughMoneyScreen();
        }

        public void AttackUpgrade()
        {
            if (_upgradeManager.TryUpgradePlayerAttack())
            {
                UpdateTexts();
                return;
            }

            ShowNotEnoughMoneyScreen();
        }

        public void ExitMenu() =>
            HideMenu();

        private void ShowNotEnoughMoneyScreen() =>
            warningCanvas.SetActive(true);
    }
}