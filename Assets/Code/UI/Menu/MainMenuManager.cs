using System;
using UnityEngine;
using Zenject;
using Code.Loader;
using Code.ScriptableObjects;
using Code.Classes;
using Code.Managers;

namespace Code.Menu
{
    public class MainMenuManager : MonoBehaviour
    {
        [SerializeField] private LevelSO levelToLoad;

        [Inject] private SceneLoader _sceneLoader;
        [Inject] private AudioManager _audioManager;

        private void Start()
        {
            _audioManager.PlayMusic(Constructors.MAIN_MENU_MUSIC);
        }

        public void LoadGame()
        {
            _sceneLoader.LoadLevel(levelToLoad);
            
            UnloadMainMenu();
        }

        private void UnloadMainMenu() =>
            _sceneLoader.UnLoadSceneByName(Constructors.MAIN_MENU_NAME);

        public void ExitGame() => Application.Quit();
    }
}