﻿using Code.DialogueSystem;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Code.UI
{
    public class GameOverCanvas : MonoBehaviour
    {
        [SerializeField] private GameObject gameOverCanvas;

        private void OnEnable() =>
            WaveLostDialogue.OnPlayerLostCanvas += ShowGameOverScreen;

        private void OnDisable() =>
            WaveLostDialogue.OnPlayerLostCanvas -= ShowGameOverScreen;

        private void ShowGameOverScreen() =>
            gameOverCanvas.SetActive(true);

        public void ExitGame() =>
            Application.Quit();

        public void RestartGame() =>
            SceneManager.LoadScene("MainScene");
    }
}