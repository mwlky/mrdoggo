﻿using System;
using UnityEngine;

namespace Code.Input
{
    public class ShowBuyMenu : MonoBehaviour
    {
        public static event Action OnOpenBuyMenu;
        public void ShowMenu() =>
            OnOpenBuyMenu?.Invoke();
    }
}