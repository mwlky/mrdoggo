using System;
using System.Collections;
using UnityEngine;

namespace Code.Managers
{
    public class BlackScreenManager : MonoBehaviour
    {
        public static event Action OnBlackScreenFullyAppeared;
        public static float AnimationTime = 3;
        
        [SerializeField] private GameObject blackScreenObject;

        private void OnEnable()
        {
            DialogueManager.OnStartGame += TriggerBlackScreen;
            GameManager.OnPlayerWonRound += TriggerBlackScreen;
            GameManager.OnPlayerLostRound += TriggerBlackScreen;
        }

        private void OnDisable()
        {
            DialogueManager.OnStartGame -= TriggerBlackScreen;
            GameManager.OnPlayerWonRound -= TriggerBlackScreen;
            GameManager.OnPlayerLostRound -= TriggerBlackScreen;
        }

        private void TriggerBlackScreen()
        {
            blackScreenObject.gameObject.SetActive(true);
            StartCoroutine(BlackScreenFullyAppeared());
            StartCoroutine(DisableBlackScreen());
        }

        private IEnumerator BlackScreenFullyAppeared()
        {
            yield return new WaitForSeconds(AnimationTime / 2);
            OnBlackScreenFullyAppeared?.Invoke();
        }

        private IEnumerator DisableBlackScreen()
        {
            yield return new WaitForSeconds(AnimationTime);
            blackScreenObject.gameObject.SetActive(false);
        }
    }
}