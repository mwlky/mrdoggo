using Code.Managers;
using UnityEngine;
using Zenject;

namespace Code.UI
{
    public class ButtonSounds : MonoBehaviour
    {
        [SerializeField] private string soundName;
        
        [Inject] private AudioManager _audioManager;
        
        public void PlayButtonSound() => _audioManager.PlayUISfx(soundName);
    }
}