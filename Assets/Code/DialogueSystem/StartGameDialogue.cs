﻿using System;
using Code.Classes;
using Code.ScriptableObjects;
using UnityEngine;
using UnityEngine.Events;

namespace Code.DialogueSystem
{
    public class StartGameDialogue : MonoBehaviour, IDialogueTrigger
    {
        public static event Action<Dialogue, IDialogueTrigger, bool> OnDialogueStart;
        
        [SerializeField] private DialogueSO welcomeDialogue;
        [SerializeField] private UnityEvent onDialogueFinishedEvent;

        private void Start() =>
            TriggerDialogue();

        private void TriggerDialogue() =>
            OnDialogueStart?.Invoke(welcomeDialogue.dialogue, this, false);

        public void DialogueFinished() =>
            onDialogueFinishedEvent?.Invoke();
    }

}