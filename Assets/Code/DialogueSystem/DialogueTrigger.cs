﻿using System;
using Code.Classes;
using Code.ScriptableObjects;
using UnityEngine;
using UnityEngine.Events;

namespace Code.DialogueSystem
{
    public class DialogueTrigger : MonoBehaviour, IDialogueTrigger
    {
        public static event Action<Dialogue, IDialogueTrigger, bool> OnDialogueStart;

        [SerializeField] private bool canPlayerRunGame;

        [SerializeField] private DialogueSO dialogueSo;
        [SerializeField] private UnityEvent onDialogueFinishedEvent;

        private void OnTriggerEnter(Collider other)
        {
            if(!other.CompareTag(Constructors.PLAYER_TAG)) return;
            
            TriggerDialogue();
            transform.LookAt(other.transform);
        }

        private void TriggerDialogue() =>
            OnDialogueStart?.Invoke(dialogueSo.dialogue, this, canPlayerRunGame);

        public void DialogueFinished() => onDialogueFinishedEvent?.Invoke();
    }
}