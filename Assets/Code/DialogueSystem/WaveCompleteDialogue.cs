using System;
using System.Collections;
using Code.Classes;
using Code.Managers;
using Code.ScriptableObjects;
using UnityEngine;
using UnityEngine.Events;

namespace Code.DialogueSystem
{
    public class WaveCompleteDialogue : MonoBehaviour, IDialogueTrigger
    {
        public static event Action<Dialogue, IDialogueTrigger, bool> OnDialogueStart;

        [SerializeField] private DialogueSO dialogue;
        [SerializeField] private float time = 2;

        [SerializeField] private UnityEvent onDialogueFinishedEvent;

        private void OnEnable()=>
            GameManager.OnPlayerWonRound += TriggerDialogue;

        private void OnDisable()=>
            GameManager.OnPlayerWonRound -= TriggerDialogue;

        private void TriggerDialogue() =>
            StartCoroutine(TriggerDialogueCoroutine());

        IEnumerator TriggerDialogueCoroutine()
        {
            yield return new WaitForSeconds(time);

            OnDialogueStart?.Invoke(dialogue.dialogue, this, false);
        }

        public void DialogueFinished() =>
            onDialogueFinishedEvent?.Invoke();
    }
}