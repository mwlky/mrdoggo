﻿using System.Collections;
using Code.CustomEvents;
using Code.Input;
using Code.Managers;
using Code.UI;
using UnityEngine;

namespace Code.Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private Transform playerStartPosition;
        [SerializeField] private Transform playerWavePosition;

        private PlayerMovement _playerMovement;
        private PlayerAnimation _playerAnimation;
        private PlayerAttack _playerAttack;
        private Rigidbody _rigidbody;

        private void Awake()
        {
            _playerMovement = GetComponent<PlayerMovement>();
            _rigidbody = GetComponent<Rigidbody>();
            _playerAnimation = GetComponent<PlayerAnimation>();
            _playerAttack = GetComponent<PlayerAttack>();
        }

        private void OnEnable()
        {
            DialogueManager.OnDialogueStarted += DisablePlayerComponents;
            ShowBuyMenu.OnOpenBuyMenu += DisablePlayerComponents;
            HandlePlayerInput.OnDisablePlayerInput += DisablePlayerComponents;
            GameManager.OnPlayerLostRound += DisablePlayerComponents;

            BuyMenuCanvas.OnBuyMenuHidden += EnablePlayerComponents;
            HandlePlayerInput.OnEnablePlayerInput += EnablePlayerComponents;

            GameManager.OnPlayerWonRound += PlayerWaveFinished;
            GameManager.OnPlayerLostRound += PlayerWaveFinished;

            GameManager.OnGameStarted += PreparePlayerToGameStart;
        }

        private void OnDisable()
        {
            DialogueManager.OnDialogueStarted -= DisablePlayerComponents;
            ShowBuyMenu.OnOpenBuyMenu -= DisablePlayerComponents;
            HandlePlayerInput.OnDisablePlayerInput -= DisablePlayerComponents;
            GameManager.OnPlayerLostRound -= DisablePlayerComponents;

            BuyMenuCanvas.OnBuyMenuHidden -= EnablePlayerComponents;
            HandlePlayerInput.OnEnablePlayerInput -= EnablePlayerComponents;

            GameManager.OnPlayerWonRound -= PlayerWaveFinished;
            GameManager.OnPlayerLostRound -= PlayerWaveFinished;

            GameManager.OnGameStarted -= PreparePlayerToGameStart;
        }

        private void DisablePlayerComponents()
        {
            _playerMovement.enabled = false;
            _rigidbody.velocity = Vector2.zero;
            _playerAnimation.ResetWalkingAnimation();
            _playerAnimation.enabled = false;
            _playerAttack.enabled = false;
        }

        private void EnablePlayerComponents()
        {
            _playerMovement.enabled = true;
            _playerAnimation.enabled = true;
            _playerAttack.enabled = true;
        }

        private void PreparePlayerToGameStart() =>
            StartCoroutine(PlayerRoundStartCoroutine());

        private IEnumerator PlayerRoundStartCoroutine()
        {
            yield return new WaitForSeconds(BlackScreenManager.AnimationTime / 2);
            transform.position = playerWavePosition.position;
        }

        private void PlayerWaveFinished() =>
            StartCoroutine(PlayerFinishedGameCoroutine());

        private IEnumerator PlayerFinishedGameCoroutine()
        {
            yield return new WaitForSeconds(BlackScreenManager.AnimationTime / 2);
            transform.position = playerStartPosition.position;
        }
    }
}