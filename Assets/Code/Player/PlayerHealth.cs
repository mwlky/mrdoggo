using System;
using Code.Classes;
using Code.CustomEvents;
using Code.Managers;
using Code.UI;
using UnityEngine;
using Zenject;

namespace Code.Player
{
    public class PlayerHealth : MonoBehaviour, IDamageable
    {
        public static event Action OnPlayerDead;

        [Inject] private AudioManager _audioManager;
        
        private HealthBar _playerHealthBar;
        private float _playerHealth;
        private float _startingHp;
        private float _maxHp;

        private void OnEnable() =>
            PlayerHealEvent.OnHealPlayer += HealPlayer;
        

        private void OnDisable() =>
            PlayerHealEvent.OnHealPlayer -= HealPlayer;

        private void Awake() =>
            _playerHealthBar = GetComponent<HealthBar>();

        public void ReceiveDamage(float damage)
        {
            if (_playerHealth <= 0) return;

            _playerHealth -= damage;
            _playerHealth = Math.Clamp(_playerHealth, 0, _startingHp);
            _playerHealthBar.UpdateHealthBar(_playerHealth, _maxHp);
            
            if (_playerHealth == 0)
                Dead();
        }

        public void SetPlayerHp(float amount)
        {
            _startingHp = amount;
            _maxHp = _startingHp;
            _playerHealth = _startingHp;
        }

        private void HealPlayer()
        {
            _playerHealth = _maxHp;
            _playerHealthBar.UpdateHealthBar(_playerHealth, _maxHp);
        }
        

        public void Dead()
        {
            _audioManager.PlayerPlayerSFX(Constructors.PLAYER_DEAD_SFX, 0);
            OnPlayerDead?.Invoke();
        }
    }
}