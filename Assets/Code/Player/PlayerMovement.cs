using Code.Input;
using UnityEngine;
using Zenject;

namespace Code.Player
{
    public class PlayerMovement : MonoBehaviour
    {
        [Inject] private GameInput _playerInput;

        [SerializeField] private float movementSpeed;
        [SerializeField] private float rotationSpeed;

        private Rigidbody _rigidbody;

        private void Awake() =>
            _rigidbody = GetComponent<Rigidbody>();
        
        private void FixedUpdate() =>
            MovePlayer();

        private void MovePlayer()
        {
            Vector3 movement = new Vector3(_playerInput.GetPlayerMovementVectorNormalized().x, 0f, _playerInput.GetPlayerMovementVectorNormalized().y);

            RotatePlayer(movement);

            _rigidbody.velocity = movement * (Time.deltaTime * movementSpeed * 10);
        }

        private void RotatePlayer(Vector3 movement)
        {
            if (movement.magnitude == 0) return;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(movement), rotationSpeed);
        }
    }
}