﻿using Code.Managers;
using Code.ScriptableObjects;
using UnityEngine;

namespace Code.Player
{
    public class PlayerStatsUpgrader : MonoBehaviour
    {
        [SerializeField] private PlayerStatsSO playerStats;

        private PlayerHealth _playerHealth;
        private PlayerAttack _playerAttack;

        private float _currentHealth;
        private float _currentAttack;

        public float CurrentPlayerHealth => _currentHealth;
        public float CurrentPlayerAttack => _currentAttack;

        private void OnEnable()
        {
            UpgradeManager.OnUpgradePlayerAttack += UpgradePlayerAttack;
            UpgradeManager.OnUpgradePlayerHealth += UpgradePlayerHealth;
        }

        private void OnDisable()
        {
            UpgradeManager.OnUpgradePlayerAttack -= UpgradePlayerAttack;
            UpgradeManager.OnUpgradePlayerHealth -= UpgradePlayerHealth;
        }

        private void Awake()
        {
            _playerAttack = GetComponent<PlayerAttack>();
            _playerHealth = GetComponent<PlayerHealth>();
        }

        private void Start()
        {
            _currentAttack = playerStats.startDamage;
            _currentHealth = playerStats.startHp;
            
            _playerHealth.SetPlayerHp(_currentHealth);
            _playerAttack.SetPlayerAttack(_currentAttack);
        }

        private void UpgradePlayerHealth(int amount)
        {
            _currentHealth += amount;
            
            _playerHealth.SetPlayerHp(_currentHealth);
        }

        private void UpgradePlayerAttack(int amount)
        {
            _currentAttack += amount;
            
            _playerAttack.SetPlayerAttack(_currentAttack);
        }
    }
}