using Code.Input;
using UnityEngine;
using Zenject;
using Code.Classes;
using Code.CustomEvents;

namespace Code.Player
{
    public class PlayerAnimation : MonoBehaviour
    {
        [Inject] private GameInput _playerInputManager;

        private Animator _animator;

        private void OnEnable()
        {
            PlayerHealth.OnPlayerDead += PlayerDeadAnimation;
            PlayerHealEvent.OnHealPlayer += ResetDeadTrigger;
        }

        private void OnDisable()
        {
            PlayerHealth.OnPlayerDead -= PlayerDeadAnimation;
            PlayerHealEvent.OnHealPlayer -= ResetDeadTrigger;
        }

        private void Awake() =>
            _animator = GetComponentInChildren<Animator>();

        private void FixedUpdate() =>
            SetWalkingAnimation();

        private void SetWalkingAnimation()
        {
            float value = _playerInputManager.GetPlayerMovementVectorNormalized().magnitude;

            _animator.SetFloat(Constructors.PLAYER_MOVEMENT, value);
        }

        private void PlayerDeadAnimation() =>
            _animator.SetTrigger(Constructors.PLAYER_DEAD_TRIGGER);

        private void ResetDeadTrigger() =>
            _animator.ResetTrigger(Constructors.PLAYER_DEAD_TRIGGER);
        
        public void ResetWalkingAnimation() => _animator.SetFloat(Constructors.PLAYER_MOVEMENT, 0);
    }
}