using Code.Input;
using UnityEngine;
using Zenject;
using Code.Classes;
using Code.Managers;
using Code.ScriptableObjects;

namespace Code.Player
{
    public class PlayerAttack : MonoBehaviour
    {
        [SerializeField] private PlayerStatsSO playerStats;

        [SerializeField] private Transform attackPoint;
        [SerializeField] private LayerMask attackLayer;

        [Inject] private GameInput _playerInput;
        [Inject] private AudioManager _audioManager;

        private Animator _animator;
        private bool _isCooldown;
        private float _cooldownTimer;
        private float _currentPlayerDamage;

        private void Awake() =>
            _animator = GetComponentInChildren<Animator>();

        private void FixedUpdate() =>
            UpdateCooldown();

        private void Start() =>
            _currentPlayerDamage = playerStats.startDamage;


        private void Update()
        {
            bool buttonTriggered = _playerInput.GetPlayerAttackButton();
            _animator.ResetTrigger(Constructors.PLAYER_ATTACK_TRIGGER);

            if (!buttonTriggered || _isCooldown) return;
            TryAttack();

            _animator.SetTrigger(Constructors.PLAYER_ATTACK_TRIGGER);
            StartCooldown();
        }

        private void TryAttack()
        {
            Collider[] hitResults = Physics.OverlapSphere(attackPoint.position, playerStats.attackRange, attackLayer);
            _audioManager.PlayerPlayerSFX(Constructors.PLAYER_ATTACK_SFX, 0);

            foreach (Collider enemy in hitResults)
            {
                enemy.gameObject.TryGetComponent(out IDamageable damageable);

                if (damageable is not null)
                    damageable.ReceiveDamage(_currentPlayerDamage);
            }
        }

        private void StartCooldown()
        {
            _isCooldown = true;
            _cooldownTimer = playerStats.attackCooldown;
        }

        private void UpdateCooldown()
        {
            if (!_isCooldown) return;

            _cooldownTimer -= Time.deltaTime;

            if (_cooldownTimer <= 0.0f)
                _isCooldown = false;
        }

        public void SetPlayerAttack(float amount) =>
            _currentPlayerDamage = amount;

        private void OnDrawGizmosSelected() =>
            Gizmos.DrawSphere(attackPoint.position, playerStats.attackRange);
    }
}