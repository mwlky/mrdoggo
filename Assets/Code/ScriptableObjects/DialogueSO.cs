﻿using Code.Classes;
using UnityEngine;

namespace Code.ScriptableObjects
{
    [CreateAssetMenu(fileName = "DialogueSO", menuName = "Dialogue", order = 0)]
    public class DialogueSO : ScriptableObject
    {
        public Dialogue dialogue;
    }
}