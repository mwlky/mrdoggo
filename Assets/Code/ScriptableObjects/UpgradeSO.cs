﻿using UnityEngine;

namespace Code.ScriptableObjects
{
    [CreateAssetMenu(fileName = "Upgrade", menuName = "Upgrade", order = 0)]
    public class UpgradeSO : ScriptableObject
    {
        public int startAttackUpgradeCost;
        public int startHealthUpgradeCost;

        public int attackDifferenceProperty;
        public int healthDifferenceProperty;
    }
}