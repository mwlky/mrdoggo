﻿using Code.Classes;
using UnityEngine;

namespace Code.ScriptableObjects
{
    [CreateAssetMenu(fileName = "Audio", menuName = "Audio", order = 0)]
    public class AudioSO : ScriptableObject
    {
        public Sound[] playerSounds, enemySounds, theme, uiSounds;
    }
}