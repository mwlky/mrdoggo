﻿using UnityEngine;

namespace Code.ScriptableObjects
{
    [CreateAssetMenu(fileName = "PlayerStats", menuName = "PlayerStats", order = 0)]
    public class PlayerStatsSO : ScriptableObject
    {
        [Header("Health")]
        public float startHp;
        
        [Header("Attack")]
        public float startDamage;
        public float attackCooldown;
        public float attackRange;
    }
}