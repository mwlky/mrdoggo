﻿using System.Collections.Generic;
using UnityEngine;

namespace Code.ScriptableObjects
{
    [CreateAssetMenu(fileName = "LevelSO", menuName = "LevelSO", order = 0)]
    public class LevelSO : ScriptableObject
    {
        public List<Scenes> ScenesList = new();

        [System.Serializable]
        public class Scenes
        {
            [SerializeField] private Object scene;
            [SerializeField] private string sceneName = "";

            public string SceneName => sceneName;
        }
    }
}