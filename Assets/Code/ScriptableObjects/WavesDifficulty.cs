﻿using UnityEngine;

namespace Code.ScriptableObjects
{
    [CreateAssetMenu(fileName = "new wave", menuName = "WavesDifficulty")]
    public class WavesDifficulty : ScriptableObject
    {
        public int wizardsAtStart;
        public int goblinsAtStart;
        public float spawnDelay;
    }
}