using System;
using UnityEngine;


namespace Code.CustomEvents
{
    public class PlayerHealEvent : MonoBehaviour
    {
        public static event Action OnHealPlayer;

        public void HealPlayer() => OnHealPlayer?.Invoke();
    }
}