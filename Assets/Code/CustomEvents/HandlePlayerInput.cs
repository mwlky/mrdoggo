﻿using System;
using UnityEngine;

namespace Code.CustomEvents
{
    public class HandlePlayerInput : MonoBehaviour
    {
        public static event Action OnDisablePlayerInput;
        public static event Action OnEnablePlayerInput;

        public void DisablePlayerInput() => OnDisablePlayerInput?.Invoke();
        public void EnablePlayerInput() => OnEnablePlayerInput?.Invoke();
    }
}