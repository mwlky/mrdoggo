﻿using UnityEngine;

namespace Code.Enemy
{
    public class EnemyStartPosition : MonoBehaviour
    {
        [SerializeField] private Transform spawnPoint;

        private void OnEnable() =>
            transform.position = spawnPoint.position;
    }
}