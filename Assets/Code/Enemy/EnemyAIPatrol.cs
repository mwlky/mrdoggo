using UnityEngine;
using UnityEngine.AI;
using Zenject;
using System.Collections.Generic;
using Code.Enums;
using Code.Player;
using Code.Classes;

namespace Code.Enemy
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class EnemyAIPatrol : MonoBehaviour
    {
        [SerializeField] private float sightRange;
        [SerializeField] private float attackRange;

        [Inject] private Transform _playerTransform;
        
        private NavMeshAgent _agent;
        private Transform _target;
        private bool _isAttacking;
        private bool _isDead;
        private EnemyPatrolState _currentPatrolState;
        private List<IEnemyStateObserver> _stateObservers = new();

        private void Awake() =>
            _agent = GetComponent<NavMeshAgent>();

        private void OnEnable()
        {
            EnemyHealth.OnEnemyDead += DeadProcedure;
            PlayerHealth.OnPlayerDead += ProcessPatrol;
        }

        private void OnDisable()
        {
            EnemyHealth.OnEnemyDead -= DeadProcedure;
            PlayerHealth.OnPlayerDead -= ProcessPatrol;
        }

        private void Start()
        {
            _target = _playerTransform;
            gameObject.SetActive(false);
        }

        private void Update()
        {
            if (_isDead) return;
            HandlePatrolState();

            if (!_isAttacking || _isDead) return;
            transform.LookAt(_target);
        }

        public void SubscribeStateChange(IEnemyStateObserver observer) => _stateObservers.Add(observer);
        public void UnsubscribeStateChange(IEnemyStateObserver observer) => _stateObservers.Remove(observer);

        public void ChangeEnemyPatrolState(EnemyPatrolState newPatrolState)
        {
            _currentPatrolState = newPatrolState;
            NotifyStateChange();
        }

        private void NotifyStateChange()
        {
            foreach (var observer in _stateObservers)
                observer.OnStateChange(_currentPatrolState);
        }

        private void HandlePatrolState()
        {
            float distance = Mathf.RoundToInt(Vector3.Distance(transform.position, _target.position));

            if (_currentPatrolState == EnemyPatrolState.Dead && !_isDead)
            {
                _isDead = true;
                SetAgentDestinationOrTargetPlayer(true, Vector3.back);

                return;
            }

            if (distance < sightRange && distance > attackRange)
            {
                ChaseTarget();
                ChangeEnemyPatrolState(EnemyPatrolState.Chasing);
                _isAttacking = false;
            }

            else if (distance <= attackRange)
            {
                if (_currentPatrolState == EnemyPatrolState.Attacking && _isAttacking) return;

                ProcessAttack();
                ChangeEnemyPatrolState(EnemyPatrolState.Attacking);
                _isAttacking = true;
            }

            else
            {
                if (_currentPatrolState == EnemyPatrolState.Patrolling) return;

                ProcessPatrol();
                ChangeEnemyPatrolState(EnemyPatrolState.Patrolling);
                _isAttacking = false;
            }
        }

        private void ChaseTarget() =>
            SetAgentDestinationOrTargetPlayer(false, _target.position);
        private void ProcessAttack() =>
            SetAgentDestinationOrTargetPlayer(true, Vector3.zero);

        private void ProcessPatrol() =>
            SetAgentDestinationOrTargetPlayer(true, Vector3.zero);

        private void SetAgentDestinationOrTargetPlayer(bool stopTargeting, Vector3 target)
        {
            _agent.SetDestination(target);
            _agent.isStopped = stopTargeting;
        }

        private void DeadProcedure(EnemyHealth sender)
        {
            if (sender.gameObject != gameObject) return;

            enabled = false;
        }

        
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, attackRange);

            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, sightRange);
        }
    }
}