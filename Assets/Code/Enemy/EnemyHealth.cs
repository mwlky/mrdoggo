using System;
using Code.Enums;
using Code.Classes;
using Code.Managers;
using Code.UI;
using UnityEngine;
using Zenject;

namespace Code.Enemy
{
    public class EnemyHealth : MonoBehaviour, IDamageable
    {
        public static event Action<EnemyHealth> OnEnemyDead;
        public static event Action<int> OnEnemyPrize; 

        [SerializeField] private float maxHealth = 100;
        [SerializeField] private int deadPrize;

        [Inject] private AudioManager _audioManager;
        
        private EnemyAIPatrol _enemyAI;
        private HealthBar _healthBar;
        private float _currentHealth;

        private void Awake()
        {
            _enemyAI = GetComponent<EnemyAIPatrol>();
            _healthBar = GetComponent<HealthBar>();
        }

        private void OnEnable() =>
            PrepareEnemy();

        public void ReceiveDamage(float damage)
        {
            if (_currentHealth <= 0) return;

            _currentHealth -= damage;
            _currentHealth = Math.Clamp(_currentHealth, 0, maxHealth);
            _healthBar.UpdateHealthBar(_currentHealth, maxHealth);

            if (_currentHealth == 0)
                Dead();
        }

        public void Dead()
        {
            _enemyAI.ChangeEnemyPatrolState(EnemyPatrolState.Dead);
            _audioManager.PlayWizardSFX(Constructors.DEAD_SFX, 0);
            OnEnemyDead?.Invoke(this);
            OnEnemyPrize?.Invoke(deadPrize);
        }

        public void PrepareEnemy()
        {
            _currentHealth = maxHealth;
            _enemyAI.ChangeEnemyPatrolState(EnemyPatrolState.Patrolling);
            _healthBar.UpdateHealthBar(_currentHealth, maxHealth);
            _enemyAI.enabled = true;
        }
    }
}