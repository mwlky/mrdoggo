﻿using System;
using Code.Managers;
using Code.Classes;
using UnityEngine;
using Zenject;

namespace Code.Enemy
{
    public class EnemyAttackDistance : MonoBehaviour
    {
        public static event Action<Vector3, float, Vector3> OnActivateParticle;

        [Inject] private Transform _playerTransform;
        [Inject] private AudioManager _audioManager;
        
        [SerializeField] private Transform projectileSpawnPoint;
        [SerializeField] private float damage;
        [SerializeField] private string sfxName;


        public void DoDamageToPlayer()
        {
            OnActivateParticle?.Invoke(projectileSpawnPoint.position, damage, _playerTransform.position);
            _audioManager.PlayWizardSFX(sfxName, 0);
        }
    }
}