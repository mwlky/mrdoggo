using Code.Classes;
using Code.Enums;
using UnityEngine;

namespace Code.Enemy
{
    public class EnemyAnimation : MonoBehaviour, IEnemyStateObserver
    {
        private Animator _animator;
        private EnemyAIPatrol _enemyAIPatrol;

        private void OnEnable() =>
            _enemyAIPatrol.SubscribeStateChange(this);

        private void OnDisable() =>
            _enemyAIPatrol.UnsubscribeStateChange(this);

        private void Awake()
        {
            _animator = GetComponentInChildren<Animator>();
            _enemyAIPatrol = GetComponent<EnemyAIPatrol>();
        }

        public void OnStateChange(EnemyPatrolState newPatrolState)
        {
            switch (newPatrolState)
            {
                case EnemyPatrolState.Attacking:
                    PlayAttackAnimation();
                    break;
                case EnemyPatrolState.Chasing:
                    PlayChasingAnimation();
                    break;
                case EnemyPatrolState.Patrolling:
                    PlayPatrolAnimation();
                    break;
                case EnemyPatrolState.Dead:
                    PlayDeadAnimation();
                    break;
            }
        }

        private void PlayAttackAnimation()
        {
            ResetAnimationsAndSetToDefault();
            _animator.SetBool(Constructors.ENEMY_ATTACKING, true);
        }

        private void PlayChasingAnimation()
        {
            ResetAnimationsAndSetToDefault();
            _animator.SetBool(Constructors.ENEMY_CHASING, true);
        }

        private void PlayPatrolAnimation()
        {
            ResetAnimationsAndSetToDefault();
            _animator.SetBool(Constructors.ENEMY_WALKING, false);
        }

        private void ResetAnimationsAndSetToDefault()
        {
            _animator.SetBool(Constructors.ENEMY_CHASING, false);
            _animator.SetBool(Constructors.ENEMY_WALKING, false);
            _animator.SetBool(Constructors.ENEMY_ATTACKING, false);
        }

        private void PlayDeadAnimation()=>
            _animator.SetTrigger(Constructors.ENEMY_DEAD_TRIGGER);
    }
}