﻿using Code.Player;
using Code.Classes;
using Code.Managers;
using UnityEngine;
using Zenject;

namespace Code.Enemy
{
    public class EnemyAttackMelee : MonoBehaviour
    {
        [Inject] private PlayerHealth _playerHealth;
        [Inject] private AudioManager _audioManager;
        
        [SerializeField] private float damage;
        [SerializeField] private string sfxName;

        public void DoDamageToPlayer()
        {
            _audioManager.PlayEnemyWarriorSFX(sfxName, 0);
            _playerHealth.ReceiveDamage(damage);
        }
    }
}