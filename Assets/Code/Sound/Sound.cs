﻿using UnityEngine;

namespace Code.Classes
{
    [System.Serializable]
    public class Sound
    {
        public string name;
        public AudioClip clip;
    }
}