﻿using System;
using System.Collections;
using System.Collections.Generic;
using Code.Classes;
using Code.DialogueSystem;
using TMPro;
using UnityEngine;

namespace Code.Managers
{
    public class DialogueManager : MonoBehaviour
    {
        public static event Action OnDialogueStarted;
        public static event Action OnStartGame;

        [Header("Dialogue Box")] [SerializeField]
        private Animator dialogueAnimator;

        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI sentenceText;

        [Space] [Header("Buttons")] [SerializeField]
        private GameObject continueButton;

        [SerializeField] private GameObject notYetButton;
        [SerializeField] private GameObject startGameButton;

        [Space] [Header("Dialogue settings")] [SerializeField]
        private float timeBetweenEachLetter;

        private Queue<string> sentences = new();

        private IDialogueTrigger _currentDialogue;

        private void OnEnable()
        {
            DialogueTrigger.OnDialogueStart += StartDialogue;
            StartGameDialogue.OnDialogueStart += StartDialogue;
            WaveCompleteDialogue.OnDialogueStart += StartDialogue;
            WaveLostDialogue.OnDialogueStart += StartDialogue;
        }

        private void OnDisable()
        {
            DialogueTrigger.OnDialogueStart -= StartDialogue;
            StartGameDialogue.OnDialogueStart -= StartDialogue;
            WaveCompleteDialogue.OnDialogueStart -= StartDialogue;
            WaveLostDialogue.OnDialogueStart -= StartDialogue;
        }

        private void StartDialogue(Dialogue dialogue, IDialogueTrigger sender, bool canPlayerStartGame)
        {
            OnDialogueStarted?.Invoke();
            
            _currentDialogue = sender;

            dialogueAnimator.SetBool(Constructors.IS_DIALOGUE_OPEN, true);
            nameText.text = dialogue.name;

            foreach (string sentence in dialogue.sentences)
                sentences.Enqueue(sentence);

            DisplayNextSentence();
            DisplayButtons(canPlayerStartGame);
        }

        public void DisplayNextSentence()
        {
            if (sentences.Count == 0)
            {
                EndDialogue();
                return;
            }

            string sentence = sentences.Dequeue();
            StopAllCoroutines();
            StartCoroutine(TypeSentenceCoroutine(sentence));
        }

        private void EndDialogue()
        {
            if (_currentDialogue is not null)
                _currentDialogue.DialogueFinished();

            dialogueAnimator.SetBool(Constructors.IS_DIALOGUE_OPEN, false);
        }

        IEnumerator TypeSentenceCoroutine(string sentence)
        {
            sentenceText.text = "";
            foreach (char letter in sentence)
            {
                sentenceText.text += letter;
                yield return new WaitForSeconds(timeBetweenEachLetter);
            }
        }

        public void StartGame() => OnStartGame?.Invoke();

        private void DisplayButtons(bool canPlayerStartGame)
        {
            continueButton.SetActive(!canPlayerStartGame);
            notYetButton.SetActive(canPlayerStartGame);
            startGameButton.SetActive(canPlayerStartGame);
        }
    }
}