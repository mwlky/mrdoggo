using System;
using Code.Classes;
using Code.Player;
using UnityEngine;
using Zenject;

namespace Code.Managers
{
    public class GameManager : MonoBehaviour
    {
        public static event Action<int> OnSpawnEnemies;
        public static event Action OnGameStarted;
        public static event Action OnPlayerWonRound;
        public static event Action OnPlayerLostRound; 

        private int _currentLevel = 1;
        private int _currentPlayerLevel;

        private bool _isLevelInProgress;

        [Inject] private AudioManager _audioManager;

        private void OnEnable()
        {
            EnemyManager.OnLevelFinished += PlayerWon;
            PlayerHealth.OnPlayerDead += PlayerLost;
            DialogueManager.OnStartGame += StartLevel;
        }

        private void OnDisable()
        {
            EnemyManager.OnLevelFinished -= PlayerWon;
            DialogueManager.OnStartGame -= StartLevel;
            PlayerHealth.OnPlayerDead -= PlayerLost;
        }

        private void Start() =>
            _audioManager.PlayMusic(Constructors.VILLAGE_MUSIC);
        
        private void StartLevel()
        {
            if (_isLevelInProgress) return;
            
            OnGameStarted?.Invoke();
            OnSpawnEnemies?.Invoke(_currentLevel);
            
            _audioManager.PlayMusic(Constructors.WAVE_MUSIC);
            
            _isLevelInProgress = true;
        }

        private void PlayerLost()
        {
            _isLevelInProgress = false;
            
            OnPlayerLostRound?.Invoke();
            _audioManager.PlayMusic(Constructors.GAME_OVER_MUSIC);
        }

        private void PlayerWon()
        {
            _isLevelInProgress = false;
            _currentLevel++;
            
            OnPlayerWonRound?.Invoke();
            _audioManager.PlayMusic(Constructors.VILLAGE_MUSIC);
        }
    }
}