﻿using System;
using Code.Classes;
using Code.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace Code.Managers
{
    public class UpgradeManager : MonoBehaviour
    {
        public static event Action<int> OnUpgradeBought;
        public static event Action<int> OnUpgradePlayerHealth;
        public static event Action<int> OnUpgradePlayerAttack; 

        [SerializeField] private UpgradeSO upgradeSo;

        [Inject] private CurrencyManager _currencyManager;
        [Inject] private AudioManager _audioManager;

        private int _currentAttackLevel = 1;
        private int _currentHealthLevel = 1;

        public int GetAttackUpgradeCost() =>
            upgradeSo.startAttackUpgradeCost + (_currentAttackLevel - 1) * upgradeSo.attackDifferenceProperty;
        
        public int GetHealthUpgradeCost() =>
            upgradeSo.startHealthUpgradeCost + (_currentHealthLevel - 1) * upgradeSo.healthDifferenceProperty;

        public bool TryUpgradePlayerHealth()
        {
            if (GetHealthUpgradeCost() > _currencyManager.CurrentGold)
            {
                _audioManager.PlayUISfx(Constructors.BUTTON_PRESSED_SFX);
                return false;
            }
            
            UpgradePlayerHealth();
            return true;
        }

        private void UpgradePlayerHealth()
        {
            _audioManager.PlayUISfx(Constructors.PURCHASE_SFX);
            
            int cost = GetHealthUpgradeCost();
            OnUpgradeBought?.Invoke(cost);

            int upgrade = upgradeSo.attackDifferenceProperty;
            OnUpgradePlayerHealth?.Invoke(upgrade);
            
            _currentHealthLevel++;
        }

        public bool TryUpgradePlayerAttack()
        {
            if (GetAttackUpgradeCost() > _currencyManager.CurrentGold) return false;
            
            UpgradePlayerAttack();
            return true;
        }

        private void UpgradePlayerAttack()
        {
            _audioManager.PlayUISfx(Constructors.PURCHASE_SFX);

            int cost = GetAttackUpgradeCost();
            OnUpgradeBought?.Invoke(cost);

            int upgrade = upgradeSo.attackDifferenceProperty;
            OnUpgradePlayerAttack?.Invoke(upgrade);
            
            _currentAttackLevel++;
        }
    }
}