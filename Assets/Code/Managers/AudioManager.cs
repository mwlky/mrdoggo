﻿using System;
using Code.Classes;
using Code.ScriptableObjects;
using UnityEngine;

namespace Code.Managers
{
    public class AudioManager : MonoBehaviour
    {
        [SerializeField] private AudioSO audioSo;
        [SerializeField] private AudioSource musicSource;
        [SerializeField] private AudioSource playerSfxSource;
        [SerializeField] private AudioSource enemyWarriorSfxSource;
        [SerializeField] private AudioSource enemyWizardSfxSource;
        [SerializeField] private AudioSource otherSfxSource;
        
        public void PlayMusic(string name)
        {
            Sound sound = Array.Find(audioSo.theme, x => x.name == name);

            musicSource.clip = sound.clip;
            musicSource.Play();
        }

        public void PlayEnemyWarriorSFX(string name, float playDelay) =>
            PlaySFX(enemyWarriorSfxSource, name, audioSo.enemySounds, playDelay);
        

        public void PlayerPlayerSFX(string name, float playDelay) =>
            PlaySFX(playerSfxSource, name, audioSo.playerSounds, playDelay);
        

        public void PlayWizardSFX(string name, float playDelay) =>
            PlaySFX(enemyWizardSfxSource, name, audioSo.enemySounds, playDelay);

        public void PlayUISfx(string name) => PlaySFX(otherSfxSource, name, audioSo.uiSounds, 0);

        
        private void PlaySFX(AudioSource source, string name, Sound[] array, float playDelay)
        {

            Sound sound = Array.Find(array, x => x.name == name);

            source.clip = sound.clip;
            source.PlayDelayed(playDelay);
        }
    }
}