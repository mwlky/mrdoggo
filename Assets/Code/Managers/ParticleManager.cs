﻿using System.Linq;
using Code.Enemy;
using Code.VFX;
using UnityEngine;

namespace Code.Managers
{
    public class ParticleManager : MonoBehaviour
    {
        [SerializeField] private GameObject[] particles;

        private void OnEnable() =>
            EnemyAttackDistance.OnActivateParticle += ActivateParticle;

        private void OnDisable() =>
            EnemyAttackDistance.OnActivateParticle -= ActivateParticle;

        private void ActivateParticle(Vector3 spawnPoint, float damage, Vector3 target)
        {
            GameObject particle = PickFirstNonActiveParticle();
            if (particle is null) return;

            ParticleMovement particleMovement = particle.GetComponent<ParticleMovement>();

            particle.transform.position = spawnPoint;
            particleMovement.TargetPosition = target;

            particle.SetActive(true);
        }

        private GameObject PickFirstNonActiveParticle() =>
            particles.FirstOrDefault(particle => !particle.activeSelf);
    }
}