﻿using System;
using System.Collections;
using System.Collections.Generic;
using Code.Enemy;
using Code.ScriptableObjects;
using UnityEngine;

namespace Code.Managers
{
    public class EnemyManager : MonoBehaviour
    {
        public static event Action OnLevelFinished; 

        [Header("Settings")]
        [SerializeField] private Transform spawnPoint;
        [SerializeField] private WavesDifficulty difficulty;
        [SerializeField] private float cooldownBetweenDeactivatingEachEnemy;
        [SerializeField] private float timeAfterEnemiesStartDisappearing;

        [Space]
        
        [Header("Enemies Arrays")]
        [SerializeField] private GameObject[] wizards;
        [SerializeField] private GameObject[] goblins;
        
        private List<EnemyHealth> _enemiesAlive = new ();
        private int _enemiesAliveAmount;

        private void OnEnable()
        {
            GameManager.OnSpawnEnemies += StartWave;
            EnemyHealth.OnEnemyDead += EnemyDeadProcedure;
        }

        private void OnDisable()
        {
            GameManager.OnSpawnEnemies -= StartWave;
            EnemyHealth.OnEnemyDead -= EnemyDeadProcedure;
        }

        private void StartWave(int level)
        {
            int wizardsAmount = level == 1 ? difficulty.wizardsAtStart : difficulty.wizardsAtStart + level;
            int goblinsAmount = level == 1 ? difficulty.goblinsAtStart: difficulty.goblinsAtStart + level;

            StartCoroutine(SpawnEnemies(wizardsAmount, wizards));
            StartCoroutine(SpawnEnemies(goblinsAmount, goblins));
        }

        private IEnumerator SpawnEnemies(int amount, GameObject[] enemyList)
        {
            for (int i = 0; i < amount; i++)
            {
                GameObject enemy = enemyList[i];

                enemy.SetActive(true);

                EnemyHealth enemyHealth = enemy.GetComponent<EnemyHealth>();
                _enemiesAlive.Add(enemyHealth);

                yield return new WaitForSeconds(difficulty.spawnDelay);
            }

            _enemiesAliveAmount = _enemiesAlive.Count;
        }

        private void EnemyDeadProcedure(EnemyHealth enemyHealth)
        {
            if(!_enemiesAlive.Contains(enemyHealth) || _enemiesAliveAmount == 0) return;
            
            _enemiesAliveAmount -= 1;
            
            if(_enemiesAliveAmount == 0)
                FinishProcedure();
        }

        private void FinishProcedure()
        {
            OnLevelFinished?.Invoke();

            StartCoroutine(ClearEnemiesCoroutine());
        }

        private IEnumerator ClearEnemiesCoroutine()
        {
            yield return new WaitForSeconds(timeAfterEnemiesStartDisappearing);
            
            _enemiesAlive.Reverse();
            
            foreach (EnemyHealth enemy in _enemiesAlive)
            {
                enemy.PrepareEnemy();
                enemy.gameObject.SetActive(false);
                yield return new WaitForSeconds(cooldownBetweenDeactivatingEachEnemy);
            }
            
            _enemiesAlive.Clear();
        }
    }
}