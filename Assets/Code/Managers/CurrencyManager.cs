﻿using Code.Enemy;
using UnityEngine;

namespace Code.Managers
{
    public class CurrencyManager : MonoBehaviour
    {
        private int _currentGold;
        public int CurrentGold => _currentGold;

        private void OnEnable()
        {
            UpgradeManager.OnUpgradeBought += DecreaseGoldAmount;
            EnemyHealth.OnEnemyPrize += IncreaseGoldAmount;
        }

        private void OnDisable()
        {
            UpgradeManager.OnUpgradeBought -= DecreaseGoldAmount;
            EnemyHealth.OnEnemyPrize -= IncreaseGoldAmount;
        }

        private void IncreaseGoldAmount(int amount) =>
            _currentGold += amount;
        
        private void DecreaseGoldAmount(int amount) =>
            _currentGold -= amount;
    }
}