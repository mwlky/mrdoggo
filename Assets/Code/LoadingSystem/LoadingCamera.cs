using UnityEngine;

namespace Code.Loader
{
    public class LoadingCamera : MonoBehaviour
    {
        private Camera _camera;

        private void Awake() =>
            _camera = GetComponentInChildren<Camera>();

        private void OnEnable()
        {
            SceneLoader.OnLoadingBegin += EnableCamera;
            SceneLoader.OnLoadingEnd += DisableCamera;
        }

        private void OnDisable()
        {
            SceneLoader.OnLoadingBegin -= EnableCamera;
            SceneLoader.OnLoadingEnd -= DisableCamera;
        }

        private void DisableCamera() =>
            _camera.gameObject.SetActive(false);

        private void EnableCamera() =>
            _camera.gameObject.SetActive(false);
    }
}