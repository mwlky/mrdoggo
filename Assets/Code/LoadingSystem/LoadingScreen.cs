using UnityEngine;

namespace Code.Loader
{
    public class LoadingScreen : MonoBehaviour
    {
        [SerializeField] private GameObject loadingScreen;
        private void OnEnable()
        {
            SceneLoader.OnLoadingBegin += ShowLoadingScreen;
            SceneLoader.OnLoadingEnd += HideLoadingScreen;
        }

        private void OnDisable()
        {
            SceneLoader.OnLoadingBegin -= ShowLoadingScreen;
            SceneLoader.OnLoadingEnd -= HideLoadingScreen;
        }

        private void ShowLoadingScreen() =>
            loadingScreen.SetActive(true);
        
        private void HideLoadingScreen() => 
            loadingScreen.SetActive(false);
    }
}