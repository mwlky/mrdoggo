using Code.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace Code.Loader
{
    public class LoadLevelAtStart : MonoBehaviour
    {
        [SerializeField] private LevelSO levelToLoadAtStart;

        [Inject] private SceneLoader _sceneLoader;

        private void Start() =>
            _sceneLoader.LoadLevel(levelToLoadAtStart);
    }
}