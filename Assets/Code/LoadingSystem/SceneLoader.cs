using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Code.ScriptableObjects;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Code.Loader
{
    public class SceneLoader : MonoBehaviour
    {
        public static event Action OnLoadingBegin;
        public static event Action OnLoadingEnd;

        [SerializeField] private Image loadingBar;

        private List<AsyncOperation> scenesToProcess = new();

        public void LoadLevel(LevelSO container)
        {
            foreach (LevelSO.Scenes scene in container.ScenesList)
                scenesToProcess.Add(SceneManager.LoadSceneAsync(scene.SceneName, LoadSceneMode.Additive));

            StartCoroutine(ProcessScenes());
        }

        public void UnloadLevel(LevelSO container)
        {
            for (int i = container.ScenesList.Count - 1; i >= 0; i--)
                scenesToProcess.Add(SceneManager.UnloadSceneAsync(container.ScenesList[i].SceneName,
                    UnloadSceneOptions.UnloadAllEmbeddedSceneObjects));

            StartCoroutine(ProcessScenes());
        }

        public void UnLoadSceneByName(string name)
        {
            scenesToProcess.Add(SceneManager.UnloadSceneAsync(name, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects));

            StartCoroutine(ProcessScenes());
        }

        private IEnumerator ProcessScenes()
        {
            OnLoadingBegin?.Invoke();

            float progress = 0f;
            float fillAmount;

            loadingBar.fillAmount = 0;

            List<AsyncOperation> scenesCopy = scenesToProcess.ToList();

            foreach (AsyncOperation scene in scenesCopy)
            {
                while (scene is not null && !scene.isDone)
                {
                    progress += scene.progress;
                    fillAmount = progress / scenesToProcess.Count;

                    loadingBar.fillAmount = fillAmount;


                    yield return null;
                }
            }

            scenesToProcess.Clear();

            OnLoadingEnd?.Invoke();
        }
    }
}