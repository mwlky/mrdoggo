using Code.Managers;
using UnityEngine;
using Zenject;

namespace Code.Zenject
{
    public class ProjectContextInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<AudioManager>().FromComponentInHierarchy().AsSingle();
        }
    }
}