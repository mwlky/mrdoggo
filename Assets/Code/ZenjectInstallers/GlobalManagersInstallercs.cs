using Code.Input;
using Code.Managers;
using UnityEngine;
using Zenject;

public class GlobalManagersInstallercs : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<GameManager>().FromComponentInHierarchy().AsSingle();
        Container.Bind<GameInput>().FromComponentInHierarchy().AsSingle();
        Container.Bind<ParticleManager>().FromComponentInHierarchy().AsSingle();
        Container.Bind<CurrencyManager>().FromComponentInHierarchy().AsSingle();
        Container.Bind<UpgradeManager>().FromComponentInHierarchy().AsSingle();
    }
}