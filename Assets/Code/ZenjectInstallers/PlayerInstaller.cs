using Code.Player;
using UnityEngine;
using Zenject;

namespace Code.Zenject
{
    public class PlayerInstaller : MonoInstaller
    {
        [SerializeField] private GameObject player;

        public override void InstallBindings()
        {
            Container.Bind<Transform>().FromComponentOn(player).AsSingle();
            Container.Bind<PlayerHealth>().FromComponentOn(player).AsSingle();
            Container.Bind<PlayerStatsUpgrader>().FromComponentOn(player).AsSingle();
            Container.Bind<Camera>().FromComponentInHierarchy().AsSingle();
        }
    }
}