using Code.Managers;
using Zenject;

namespace Code.Zenject
{
    public class CanvasesInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<DialogueManager>().FromComponentInHierarchy(false).AsSingle();
        }
    }
}