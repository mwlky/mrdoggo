using Code.Input;
using Code.Loader;
using Code.Managers;
using Zenject;

namespace Code.Zenject
{
    public class MainSceneInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<SceneLoader>().FromComponentInHierarchy().AsSingle();
        }
    }
}