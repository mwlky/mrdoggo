using Code.Player;
using UnityEngine;

namespace Code.VFX
{
    public class ParticleDamage : MonoBehaviour
    {

        [SerializeField] private float damage;
        [SerializeField] private float destroyTimeInSeconds;

        private void OnCollisionEnter(Collision collision)
        {
            collision.gameObject.TryGetComponent(out PlayerHealth playerHealth);

            if (playerHealth is not null)
            {
                playerHealth.ReceiveDamage(damage);
                DeactivateGameObject();
            }

            Invoke(nameof(DeactivateGameObject), destroyTimeInSeconds);
        }

        private void DeactivateGameObject() =>
            gameObject.SetActive(false);
    }
}