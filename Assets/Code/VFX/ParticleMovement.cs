using UnityEngine;

namespace Code.VFX
{
    public class ParticleMovement : MonoBehaviour
    {
        [SerializeField] private float speed;

        public Vector3 TargetPosition
        {
            set => _targetPosition = value;
        }

        private Vector3 _targetPosition = Vector3.zero;

        private void Update() =>
            MoveTowardsTarget();

        private void MoveTowardsTarget()
        {
            transform.position = Vector3.Lerp(transform.position, _targetPosition, speed * Time.deltaTime);
            transform.LookAt(_targetPosition);
        }
    }
}