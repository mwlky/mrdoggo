namespace Code.Classes
{
    public class Constructors
    {
        // Dialogue Animation
        public const string IS_DIALOGUE_OPEN = "IsOpen";

        public const string PLAYER_TAG = "Player";
        
        // Player Animator
        public const string PLAYER_MOVEMENT = "movementX";
        public const string IS_PLAYER_ATTACKING = "isAttacking";
        public const string PLAYER_ATTACK_TRIGGER = "Attack";
        public const string PLAYER_DEAD_TRIGGER = "Dead";
        
        // Enemy Animator
        public const string ENEMY_CHASING = "IsChasing";
        public const string ENEMY_WALKING = "IsWalking";
        public const string ENEMY_ATTACKING = "IsAttacking";
        public const string ENEMY_DEAD_TRIGGER = "Dead";
        
        // Scenes
        public const string MAIN_MENU_NAME = "MainMenu";

        // Music
        public const string MAIN_MENU_MUSIC = "MenuMusic";
        public const string WAVE_MUSIC = "WaveMusic";
        public const string VILLAGE_MUSIC = "VillageMusic";
        public const string GAME_OVER_MUSIC = "GameOver";
        
        // SFXs
        public const string PLAYER_ATTACK_SFX = "Attack";
        public const string DEAD_SFX = "EnemyDead";
        public const string PLAYER_DEAD_SFX = "PlayerDead";
        public const string PURCHASE_SFX = "PurchaseButton";
        public const string BUTTON_PRESSED_SFX = "ButtonPressed";
        
        //UI Animations
        public const string BUY_MENU_OPEN = "IsOpen";
    }
}