using UnityEngine;

namespace Code.Input
{
    public class GameInput : MonoBehaviour
    {
        private PlayerInputAction _playerInputAction;

        private void Awake()
        {
            _playerInputAction = new PlayerInputAction();
            _playerInputAction.Player.Enable();
        }

        public bool GetPlayerAttackButton() =>
            _playerInputAction.Player.Attack.triggered;
        
        public Vector2 GetPlayerMovementVectorNormalized()
        {
            Vector2 input = _playerInputAction.Player.Movement.ReadValue<Vector2>();
            input = input.normalized;

            return input;
        }
    }
}