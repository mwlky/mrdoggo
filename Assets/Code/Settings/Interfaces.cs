using Code.Enums;

namespace Code.Classes
{
    public interface IEnemyStateObserver
    {
        void OnStateChange(EnemyPatrolState newPatrolState);
    }
    
    public interface IDamageable
    {
        public void ReceiveDamage(float damage);
    }

    public interface IDialogueTrigger
    {
        public void DialogueFinished();
    }
}
