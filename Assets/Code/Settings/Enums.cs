namespace Code.Enums
{
    public enum EnemyPatrolState
    {
        Patrolling,
        Chasing,
        Attacking,
        Dead
    }
}