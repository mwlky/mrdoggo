                           @               0.0.0 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙   Ŕ           1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               \     ˙˙˙˙               H r   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                     Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                        \       ŕyŻ     `       ŕ                                                                                                                                                ŕyŻ                                                                                    DialogueManager a  using System;
using System.Collections;
using System.Collections.Generic;
using Code.Classes;
using Code.DialogueSystem;
using TMPro;
using UnityEngine;

namespace Code.Managers
{
    public class DialogueManager : MonoBehaviour
    {
        public static event Action OnDialogueStarted;
        public static event Action OnStartGame;

        [Header("Dialogue Box")] [SerializeField]
        private Animator dialogueAnimator;

        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI sentenceText;

        [Space] [Header("Buttons")] [SerializeField]
        private GameObject continueButton;

        [SerializeField] private GameObject notYetButton;
        [SerializeField] private GameObject startGameButton;

        [Space] [Header("Dialogue settings")] [SerializeField]
        private float timeBetweenEachLetter;

        private Queue<string> sentences = new();

        private IDialogueTrigger _currentDialogue;

        private void OnEnable()
        {
            DialogueTrigger.OnDialogueStart += StartDialogue;
            StartGameDialogue.OnDialogueStart += StartDialogue;
            WaveCompleteDialogue.OnDialogueStart += StartDialogue;
            WaveLostDialogue.OnDialogueStart += StartDialogue;
        }

        private void OnDisable()
        {
            DialogueTrigger.OnDialogueStart -= StartDialogue;
            StartGameDialogue.OnDialogueStart -= StartDialogue;
            WaveCompleteDialogue.OnDialogueStart -= StartDialogue;
            WaveLostDialogue.OnDialogueStart -= StartDialogue;
        }

        private void StartDialogue(Dialogue dialogue, IDialogueTrigger sender, bool canPlayerStartGame)
        {
            OnDialogueStarted?.Invoke();
            
            _currentDialogue = sender;

            dialogueAnimator.SetBool(Constructors.IS_DIALOGUE_OPEN, true);
            nameText.text = dialogue.name;

            foreach (string sentence in dialogue.sentences)
                sentences.Enqueue(sentence);

            DisplayNextSentence();
            DisplayButtons(canPlayerStartGame);
        }

        public void DisplayNextSentence()
        {
            if (sentences.Count == 0)
            {
                EndDialogue();
                return;
            }

            string sentence = sentences.Dequeue();
            StopAllCoroutines();
            StartCoroutine(TypeSentenceCoroutine(sentence));
        }

        private void EndDialogue()
        {
            if (_currentDialogue is not null)
                _currentDialogue.DialogueFinished();

            dialogueAnimator.SetBool(Constructors.IS_DIALOGUE_OPEN, false);
        }

        IEnumerator TypeSentenceCoroutine(string sentence)
        {
            sentenceText.text = "";
            foreach (char letter in sentence)
            {
                sentenceText.text += letter;
                yield return new WaitForSeconds(timeBetweenEachLetter);
            }
        }

        public void StartGame() => OnStartGame?.Invoke();

        private void DisplayButtons(bool canPlayerStartGame)
        {
            continueButton.SetActive(!canPlayerStartGame);
            notYetButton.SetActive(canPlayerStartGame);
            startGameButton.SetActive(canPlayerStartGame);
        }
    }
}                          DialogueManager    Code.Managers   